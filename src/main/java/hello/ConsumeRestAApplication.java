package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ConsumeRestAApplication {

	private static Logger Log = LoggerFactory.getLogger(ConsumeRestAApplication.class);

	public static void main(String[] args) {
		Log.info("Iniciando la aplicación principal.");
		SpringApplication.run(ConsumeRestAApplication.class, args);
		Log.info("Terminando la aplicacion principal.");
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			Quote quote = restTemplate.getForObject("https://gturnquist-quoters.cfapps.io/api/random", Quote.class);
			Log.info(quote.toString());
		};
	}

}
